@extends('layout')

@section('title', "A propos de LGI3")

@section("content")
<h1>A propos de LIG3</h1>
<p>

    Lorem ipsum dolor, sit amet consectetur adipisicing elit. Quis labore molestias dolores aliquam omnis provident iure asperiores incidunt suscipit nisi aspernatur, et corporis sint nostrum aut ratione! Cupiditate, autem ea.
    Lorem ipsum dolor sit amet consectetur adipisicing elit. Illum dolorum doloremque temporibus dolores ea tempore expedita. Unde natus sapiente placeat error est, rerum, sint, iure illum quia quam consequatur veritatis.
</p>

<p>
    Lorem ipsum dolor sit amet consectetur adipisicing elit. Sit, deserunt cupiditate eveniet tenetur saepe porro doloremque iure culpa magnam ex, ipsa neque ipsam suscipit maiores dolores sapiente, sequi distinctio dolore?
    Lorem ipsum dolor sit, amet consectetur adipisicing elit. Eaque, vel doloribus. Dignissimos, sit? Dolorum ut assumenda dolorem cum minima laudantium consectetur atque, asperiores provident quos dolores amet tempore, eligendi debitis.
</p>
@endsection